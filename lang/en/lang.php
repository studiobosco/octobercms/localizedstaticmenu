<?php return [
  'plugin' => [
      'name' => 'Localized static menu',
      'description' => 'Provides a localized static menu.',
  ],
  'component' => [
    'localized_static_menu_name' => 'Localized static menu',
    'localized_static_menu_description' => 'Outputs a localized menu in a CMS layout.',
    'localized_static_menu_code_prefix_name' => 'Menu code prefix',
    'localized_static_menu_code_prefix_description' => 'Specify a code prefix of the menu the component should output. Language codes will be attached to the code prefix e.g. "main_" will become "main_en".'
  ],
];